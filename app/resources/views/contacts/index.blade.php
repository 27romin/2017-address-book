@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Contacts</div>
				<div class="panel-body">
				@if(Auth::user()->admin)
				<a class="btn btn-xs btn-primary" href="{{ url('contact/create')}}">New Contact</a>
				@endif
					@include('layouts.errors')
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Full Name</th>
								<th>Phone Number</th>
								<th>Email Address</th>
								@if(Auth::user()->admin)
								<th></th>
								<th></th>
								@endif
							</tr>
						</thead>
						<tbody>
							@foreach($contacts as $contact)
							<tr>
								<td>{{$contact->person->firstname}} {{$contact->person->surname}}</td>
								<td>{{$contact->phone->number}}</td>
								<td>{{$contact->email->address}}</td>
								@if(Auth::user()->admin)
								<td><a class="btn btn-xs btn-warning" href="{{ route('contact.edit', $contact->id) }}">Edit</a></td>
								<td>
									<a href="#" class="btn btn-xs btn-danger" onclick="$(this).find('form').submit();">
										Delete
										<form action="/app/contact/{{ $contact->id }}" method="POST">
											<input type="hidden" name="_method" value="DELETE">
											{{csrf_field()}}
										</form>
									</a>
								</td>
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
