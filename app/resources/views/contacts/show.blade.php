@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Contacts</div>
				<div class="panel-body">
					<a href="/app" class="btn btn-xs btn-primary">See All</a>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Full Name</th>
								<th>Phone Number</th>
								<th>Email Address</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{$contact->person->firstname}} {{$contact->person->surname}}</td>
								<td>{{$contact->phone->number}}</td>
								<td>{{$contact->email->address}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
