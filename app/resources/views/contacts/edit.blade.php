@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Contact</div>
				<div class="panel-body">
				@include('layouts.errors')
					<form method="POST" action="{{ route('contact.update', $contact->id) }}">
						{{method_field('PATCH')}}
						{{csrf_field()}}
						<div class="form-group">
							<label>Firstname</label>
							<input type="text" class="form-control" id="firstname" name="firstname" value="{{ $contact->person->firstname }}">
						</div>
						<div class="form-group">
							<label>Surname</label>
							<input type="text" class="form-control" id="surname" name="surname" value="{{ $contact->person->surname }}">
						</div>
						<div class="form-group">
							<label>Phone Number</label>
							<input type="text" class="form-control" id="phonenumber" name="phonenumber" value="{{ $contact->phone->number }}">
						</div>
						<div class="form-group">
							<label>Email Address</label>
							<input type="text" class="form-control" id="emailaddress" name="emailaddress" value="{{ $contact->email->address }}">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
