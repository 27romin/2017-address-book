@extends('layouts.app')

@section('content')
@if(Auth::user()->admin)
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Add a Contact</div>
				<div class="panel-body">
				@include('layouts.errors')
					<form method="POST" action="{{ route('contact.store') }}">
						{{csrf_field()}}
						<div class="form-group">
							<label>Firstname</label>
							<input type="text" class="form-control" id="firstname" name="firstname">
						</div>
						<div class="form-group">
							<label>Surname</label>
							<input type="text" class="form-control" id="surname" name="surname">
						</div>
						<div class="form-group">
							<label>Phone Number</label>
							<input type="text" class="form-control" id="phonenumber" name="phonenumber">
						</div>
						<div class="form-group">
							<label>Email Address</label>
							<input type="text" class="form-control" id="emailaddress" name="emailaddress">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Create</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@endsection
