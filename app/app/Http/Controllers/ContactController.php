<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Phone;
use App\Email;
use App\Person;
use Auth;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::orderBy('created_at', 'desc')->get();
        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->admin){
            return view('contacts.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->admin){

            $this->validate($request, 
                [
                'firstname' => 'required|min:3|max:10',
                'surname' => 'required|min:3|max:10',
                'emailaddress' => 'required|email|unique:emails,address',
                'phonenumber' => 'required|regex:/(0)[0-9]{10}/|unique:phones,number',
                ], []);

            $person = Person::create([
                'firstname' => $request->input('firstname'),
                'surname' => $request->input('surname'),
                ]);

            $email = Email::create(['address' => $request->input('emailaddress')]);
            $phone = Phone::create(['number' => $request->input('phonenumber')]);
            $contact = Contact::create([
                'person_id' => $person->id,
                'email_id' => $email->id,
                'phone_id' => $phone->id
                ]);
            return redirect()->route('contact.show', ['id' => $contact->id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::find($id);
        return $contact ? view('contacts.show', compact('contact')) : redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->admin){
            $contact = Contact::find($id);
            return $contact ? view('contacts.edit', compact('contact')) : redirect('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
            'firstname' => 'required|min:3|max:10',
            'surname' => 'required|min:3|max:10',
            'emailaddress' => 'required|email',
            'phonenumber' => 'required|regex:/(0)[0-9]{10}/',
            ], []);

        $contact = Contact::find($id);
        Person::where('id', $contact->person_id)->update([
            'firstname' => $request->input('firstname'), 
            'surname' => $request->input('surname'), 
            ]);

        Email::where('id', $contact->email_id)->update([
            'address' => $request->input('emailaddress'),
            ]);

        Phone::where('id', $contact->phone_id)->update([
            'number' => $request->input('phonenumber'),
            ]);

        return redirect()->route('contact.show', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->admin){
            $contact = Contact::find($id);
            $contact->delete();
            return redirect('/');
        }
    }
}
