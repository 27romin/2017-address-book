<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
	protected $guarded = ['id'];
	
    public function contact()
    {
    	return $this->belongsTo(Contact::class);
    }
}
