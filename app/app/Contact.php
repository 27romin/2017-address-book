<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	protected $guarded = ['id'];

    public function email()
    {
    	return $this->belongsTo(Email::class);
    }

    public function phone()
    {
    	return $this->belongsTo(Phone::class);
    }

    public function person()
    {
    	return $this->belongsTo(Person::class);
    }
}
