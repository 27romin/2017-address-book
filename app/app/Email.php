<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
	protected $guarded = ['id'];
	
    public function contact()
    {
    	return $this->belongsTo(Contact::class);
    }
}
