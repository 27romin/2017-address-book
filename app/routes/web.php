<?php
Auth::routes();
Route::resource('contact', 'ContactController');
Route::get('/', 'ContactController@index');
Route::get('/home', 'ContactController@index');